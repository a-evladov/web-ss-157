package model

import (
	"github.com/jinzhu/gorm"
)

type FcmNotificationInfo struct {
	ID                uint64 `gorm:"primary_key"`
	RedisID           uint64
	MessageID         uint64
	Sended            uint64
	Error             uint64
	Success           uint64
	NotRegistered     uint64
	ChatID            string `gorm:"size:200"`
	Text              string
	ChannelName       string
	TotalSubscription uint64
	DateCreated       uint64
	Seen              uint64
}

func (FcmNotificationInfo) TableName() string {
	return "FcmNotificationInfo"
}

func (fni *FcmNotificationInfo) GetID() uint64 {
	return fni.ID
}

func (fni *FcmNotificationInfo) BeforeCreate(*gorm.DB) error {
	return nil
}

const (
	KeyFcmNotificationInfoID            = "id"
	KeyFcmNotificationInfoRedisID       = "redis_id"
	KeyFcmNotificationInfoMessageID     = "message_id"
	KeyFcmNotificationInfoSended        = "sended"
	KeyFcmNotificationInfoError         = "error"
	KeyFcmNotificationInfoSuccess       = "success"
	KeyFcmNotificationInfoNotRegistered = "not_registered"
	KeyFcmNotificationInfoChatID        = "chat_id"
)

func (fni *FcmNotificationInfo) GetAffectedFields(withID bool, affectedFields ...string) map[string]interface{} {
	fv := make(map[string]interface{})
	for _, fieldName := range affectedFields {
		switch fieldName {
		case KeyFcmNotificationInfoID:
			if withID {
				fv[fieldName] = fni.ID
			}
			break
		case KeyFcmNotificationInfoRedisID:
			fv[fieldName] = fni.RedisID
			break
		case KeyFcmNotificationInfoMessageID:
			fv[fieldName] = fni.MessageID
			break
		case KeyFcmNotificationInfoSended:
			fv[fieldName] = fni.Sended
			break
		case KeyFcmNotificationInfoError:
			fv[fieldName] = fni.Error
			break
		case KeyFcmNotificationInfoSuccess:
			fv[fieldName] = fni.Success
			break
		case KeyFcmNotificationInfoNotRegistered:
			fv[fieldName] = fni.NotRegistered
			break
		case KeyFcmNotificationInfoChatID:
			fv[fieldName] = fni.ChatID
			break

		}
	}
	return fv
}
