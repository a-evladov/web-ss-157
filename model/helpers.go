package model

type PageResponse struct {
	CurrentPage  int `json:"CurrentPage"`
	CountObjects int `json:"CountObjects"`
	MaxObjects   int `json:"MaxObjects"`
}

type PageRequest struct {
	Page  int `json:"Page"`
	Count int `json:"Count"`
}
