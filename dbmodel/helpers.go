package dbmodel

import (
	"bitbucket.org/a-evladov/web-stat/model"
	"github.com/jinzhu/gorm"
)

type TableWithID interface {
	GetID() uint64
	BeforeCreate(*gorm.DB) error
	GetAffectedFields(withID bool, affectedFields ...string) map[string]interface{}
}

func retrieveDatabaseRecordsWithPage(result interface{}, sql string, page *model.PageRequest, params ...interface{}) error {
	var err error

	err = g.Raw(sql, params...).Offset(page.Count * (page.Page - 1)).Limit(page.Count).Scan(result).Error

	if err != nil {
		return err
	}

	return nil
}
