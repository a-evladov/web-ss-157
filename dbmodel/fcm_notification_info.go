package dbmodel

import (
	"bitbucket.org/a-evladov/web-stat/model"
	"fmt"
)

type FNIDatabase struct{}

var (
	FNIRepository = FNIDatabase{}
)

func (_ *FNIDatabase) GetStats(page *model.PageRequest, where string) ([]*model.FcmNotificationInfo, error) {
	result := make([]*model.FcmNotificationInfo, 0)
	if where != "" {
		where = "WHERE " + where
	}
	query := fmt.Sprintf(`select 
	tbl.sended,
	tbl.success,
	tbl."error",
	tbl.not_registered,
	channel.name as channel_name, 
	channel.chat_id as chat_id, 
	channel.total_subscribers as total_subscription, 
	msg.id_search as message_id, 
	msg."text", 
	msg.date_created, 
	msg.seen 
from (
	select 
		sum(fcm.sended) as sended, 
		sum(fcm.success) as success, 
		sum(fcm.error) as "error", 
		sum(fcm.not_registered) as not_registered, 
		fcm.chat_id, 
		fcm.message_id 
	from public."FcmNotificationInfo" fcm
	group by fcm.message_id, fcm.chat_id
) tbl
JOIN public."Channel" channel ON tbl.chat_id = channel.chat_id 
JOIN public."ChatMessage" msg ON tbl.message_id=msg.id_search and tbl.chat_id = msg.complex_chat_id
%s
order by msg.date_created desc`, where)

	err := retrieveDatabaseRecordsWithPage(&result, query, page)

	return result, err
}
