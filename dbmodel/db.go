package dbmodel

import (
	"bitbucket.org/a-evladov/web-stat/model"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/spf13/viper"
)

func NewDB() *DB {
	db := &DB{
		Username: viper.GetString("db.username"),
		Password: viper.GetString("db.password"),
		Hostname: viper.GetString("db.hostname"),
		Port:     viper.GetInt64("db.port"),
		Database: viper.GetString("db.database"),
	}
	return db
}

type DB struct {
	Username string
	Password string
	Hostname string
	Port     int64
	Database string
}

var (
	g      *gorm.DB
	tables []interface{}
)

func init() {
	tables = append(tables,
		model.FcmNotificationInfo{},
	)
}

func Init() error {
	dbc := NewDB()
	if dbc.Hostname == "" {
		return errors.New("db.hostname")
	}
	if dbc.Port == 0 {
		return errors.New("db.port")
	}
	if dbc.Username == "" {
		return errors.New("db.username")
	}
	if dbc.Password == "" {
		return errors.New("db.password")
	}
	if dbc.Database == "" {
		return errors.New("db.database")
	}
	err := NewEngine(dbc)
	if err != nil {
		return err
	}

	err = g.DB().Ping()
	if err != nil {
		return err
	}

	//g.Callback().Create().Remove("gorm:create_time_stamp")
	//g.Callback().Update().Remove("gorm:update_time_stamp")
	return nil
}

func NewEngine(dbc *DB) error {
	var err error
	connectString := fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s", dbc.Hostname, dbc.Port, dbc.Username, dbc.Database, dbc.Password)
	g, err = gorm.Open("postgres", connectString)

	if err != nil {
		return err
	}
	return nil
}
