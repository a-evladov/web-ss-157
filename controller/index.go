package controller

import (
	"bitbucket.org/a-evladov/web-stat/dbmodel"
	"bitbucket.org/a-evladov/web-stat/model"
	"fmt"
	"gopkg.in/macaron.v1"
	"log"
	"strconv"
)

func Index(ctx *macaron.Context) {
	ctx.Data["Name"] = "jeremy"
	ctx.HTML(200, "index")
}

func Stat(ctx *macaron.Context) {
	count := 10
	cnt, err := strconv.Atoi(ctx.Query("count"))
	if err == nil {
		count = cnt
	}

	page := 1
	p, err := strconv.Atoi(ctx.Query("page"))
	if err == nil {
		page = p
	}

	pageRequest := &model.PageRequest{
		Page:  page,
		Count: count,
	}

	dateStart := 0
	dateEnd := 0
	if t := ctx.Query("date_start"); t != "" {
		s, e := strconv.Atoi(t)
		if e == nil {
			if s > 0 {
				dateStart = s
			}
		}
	}
	if t := ctx.Query("date_end"); t != "" {
		s, e := strconv.Atoi(t)
		if e == nil {
			if s > 0 {
				dateEnd = s
			}
		}
	}

	channel := ctx.Query("channel")

	where := ""
	if channel != "" {
		where = where + fmt.Sprintf(` channel."name" LIKE '%s%%'`, channel)
	}

	if dateStart > 0 {
		if where != "" {
			where = where + " AND"
		}
		where = where + fmt.Sprintf(` msg.date_created >= %d`, dateStart)
	}

	if dateEnd > 0 {
		if where != "" {
			where = where + " AND"
		}
		where = where + fmt.Sprintf(` msg.date_created <= %d`, dateEnd)
	}

	log.Print(where)
	res, err := dbmodel.FNIRepository.GetStats(pageRequest, where)

	ctx.Data["Result"] = res
	ctx.Data["Error"] = err

	if ctx.Query("type") == "json" {
		result := map[string]interface{}{
			"result": res,
			"page":   page,
			"count":  count,
		}
		ctx.JSON(200, result)
	} else {
		ctx.HTML(200, "index")
	}

}
