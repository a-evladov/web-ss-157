module bitbucket.org/a-evladov/web-stat

go 1.13

require (
	github.com/jinzhu/gorm v1.9.12
	github.com/spf13/viper v1.6.2
	gopkg.in/macaron.v1 v1.3.5
)
