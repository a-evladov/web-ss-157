package main

import (
	"bitbucket.org/a-evladov/web-stat/config"
	"bitbucket.org/a-evladov/web-stat/controller"
	"bitbucket.org/a-evladov/web-stat/dbmodel"
	"github.com/spf13/viper"
	"gopkg.in/macaron.v1"
	"html/template"
	"log"
	"strings"
	"time"
)

var (
	funcMap = template.FuncMap{
		// The name "title" is what the function will be called in the template text.
		"title": strings.Title,
		"time":  toTime,
	}
)

func toTime(v uint64) string {
	tm := time.Unix(int64(v/1000), 0)
	return tm.String()
}

func main() {

	config.Init()
	err := dbmodel.Init()
	if err != nil {
		log.Fatal(err)
	}

	m := macaron.Classic()

	m.Use(macaron.Renderer(macaron.RenderOptions{
		Directory:       "templates",                 // Specify what path to load the templates from.
		Extensions:      []string{".tmpl", ".html"},  // Specify extensions to load for templates.
		Funcs:           []template.FuncMap{funcMap}, // Specify helper function maps for templates to access.
		Charset:         "UTF-8",                     // Sets encoding for json and html content-types. Default is "UTF-8".
		IndentJSON:      true,                        // Output human readable JSON
		IndentXML:       true,                        // Output human readable XML
		HTMLContentType: "text/html",                 // Output XHTML content type instead of default "text/html"
	}))
	m.Get("/", controller.Stat)
	m.Run(viper.GetString("host"), viper.GetInt("port"))
}
